# Changelog

## v2.0.0
* Complete rework of the template
* Use Gravenbock initials

## v1.1.1

* Update README
* Add some comments to *main.tex*
* Use english for the example properties

## v1.1.0

* Update paper format
* Make draft option more useful
* Supress underfull vspace warnings

## v1.0.0

Initial release.
