# book_template

Template for a DIN A5 book layout for wannabe authors, which can used for novels. It uses the [Gravenbock initials](https://gitlab.com/wangenau/gravenbock).

## Usage

To use the template one only has to change the properties inside the [main.tex](main.tex) file and add their own files to the [contents](contents) folder.

You can download the template from the [releases section](https://gitlab.com/wangenau/book_template/-/releases). It is best to select the `minimal_book_template.zip`.

When using `git`, you can clone the repository using
```terminal
git clone --recurse-submodules https://gitlab.com/wangenau/book_template
```

You have to compile the project with XeLaTeX or LuaLaTeX!

## Credits

This template is a inspired by Fifo F.'s [Fiction novel template](https://de.overleaf.com/latex/templates/fiction-novel-template/wsbhgwncfczy).
